using System;
using RunasDev.Core.SaveSystem;
using RunasDev.UI;
using UnityEngine;

public class Root : MonoBehaviour
{
    [SerializeField]
    private int startTimerCount;

    [SerializeField]
    private TimerMenu timerMenu;
    
    [SerializeField]
    private TimerWindow timerWindow;
    
    private TimerHandler _timerHandler;

    private void Start()
    {
        _timerHandler = new TimerHandler(Time.time);
        
        for (int i = 0; i < startTimerCount; i++)
        {
            _timerHandler.AddTimer(new Timer(0, _timerHandler));
        }
        
        timerMenu.Init(_timerHandler, timerWindow);
        timerMenu.Open();
    }

    private void Update()
    {
        _timerHandler.UpdateTimers(Time.time);
    }
}