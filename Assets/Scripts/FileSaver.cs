﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace RunasDev.Core.SaveSystem
{
    public class FileSaver
    {
        private readonly string _savePath; // путь к файлу сохранения

        private readonly string _fileName;


        public FileSaver(string fileName = "save.json")
        {
            _fileName = fileName;
            _savePath = Application.persistentDataPath + "/SaveData";
        }

        public T LoadAndParse<T>()
        {
            return JsonUtility.FromJson<T>(Load());
        }

        public string Load()
        {
            try
            {
                if (Directory.Exists(_savePath))
                { 
                    return File.ReadAllText(_savePath + "/" + _fileName);
                }
                else
                {
                    Directory.CreateDirectory(_savePath);
                }
            }
            catch (Exception exception)
            {
                Debug.LogError(exception);
                //throw;
            }

            return null;
        }

        public void Save(object obj)
        {
            if (!Directory.Exists(_savePath))
                Directory.CreateDirectory(_savePath);

            File.WriteAllText(_savePath + "/" + _fileName, JsonUtility.ToJson(obj));
        }
    }
}