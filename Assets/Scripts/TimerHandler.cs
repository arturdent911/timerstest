using System.Collections.Generic;

public class TimerHandler
{
    private readonly List<Timer> _timers;
    private float _time;

    public float CurrentTime => _time;

    public IReadOnlyList<Timer> Timers => _timers;

    public TimerHandler(float time)
    {
        _timers = new List<Timer>();
        _time = time;
    }
    
    public void UpdateTimers(float time)
    {
        _time = time;

        foreach (var timer in Timers)
        {
            if (timer.IsStart && timer.TimeLeft <= 0)
            {
                timer.Stop();
            }
        }
    }
    
    public void AddTimer(Timer timer)
    {
        _timers.Add(timer);
    }
    
    public void RemoveTimer(Timer timer)
    {
        _timers.Remove(timer);
    }
}