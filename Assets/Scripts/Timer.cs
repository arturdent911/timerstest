using System;
using Unity.VisualScripting;
using UnityEngine;

public class Timer
{
    [Serializable]
    public class TimerData
    {
        public float timeLeft;

        public float startTime;

        public bool isStart;
    }
    
    private readonly TimerHandler _timerHandler;
    
    private float _timeLeft;

    private float _startTime;

    private bool _isStart;

    public float TimeLeft
    {
        get => _isStart? _timeLeft - (_timerHandler.CurrentTime - _startTime) : _timeLeft;
        set => _timeLeft = Mathf.Clamp( value + (_isStart ? (_timerHandler.CurrentTime - _startTime) : 0f), 0f, float.MaxValue); 
    }

    public bool IsStart => _isStart;

    public event Action OnTimerEnd;

    public Timer(TimerData data, TimerHandler timerHandler) : this(data.timeLeft, timerHandler)
    {
        _startTime = data.startTime;
        _isStart = IsStart;
    }

    public Timer(float timeLeft, TimerHandler timerHandler)
    {
        _timeLeft = timeLeft;
        _timerHandler = timerHandler;
    }

    public void Start()
    {
        if(_isStart)
            return;
        
        _startTime = _timerHandler.CurrentTime;
        _isStart = true;
    }

    public void Stop()
    {
        if(!_isStart)
            return;
        
        _timeLeft -= _timerHandler.CurrentTime - _startTime;
        _startTime = 0;
        if (_timeLeft <= 0)
        {
            _timeLeft = 0;
            OnTimerEnd?.Invoke();
        }
        _isStart = false;
    }

    public TimerData GetSaveData()
    {
        return new TimerData
        {
            timeLeft = TimeLeft,
            startTime = _isStart? _startTime : 0f,
            isStart = _isStart
        };
    }
}