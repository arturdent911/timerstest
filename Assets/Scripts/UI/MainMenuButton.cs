using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace RunasDev.UI
{
    public class MainMenuButton : MonoBehaviour
    {
        [SerializeField] 
        private Button button;

        [SerializeField] 
        private RectTransform rectTransform;
        
        private Timer _timer;

        public Action OnClick;

        public Timer Timer => _timer;

        public void Init(Timer timer)
        {
            _timer = timer;
            
            rectTransform.anchorMax = Vector2.one + Vector2.left * 3;
            rectTransform.anchorMin = Vector2.left * 3;

            rectTransform.DOAnchorMin(Vector2.zero, 0.4f);
            rectTransform.DOAnchorMax(Vector2.one, 0.4f);
            
            button.onClick.AddListener(() => OnClick?.Invoke());
            Timer.OnTimerEnd += OnTimerEnd;
        }

        private void OnTimerEnd()
        {
            transform.DOPunchScale(new Vector3(0.1f, 0.1f, 0), 2, 3);
        }

        public void DeInit()
        {
            rectTransform.DOAnchorMax(Vector2.one + Vector2.left * 3, 0.4f);
            rectTransform.DOAnchorMin(Vector2.left * 3, 0.4f);

            
            OnClick = null;
            button.onClick.RemoveAllListeners();
            Timer.OnTimerEnd -= OnTimerEnd;
            _timer = null;
        }
    }
}