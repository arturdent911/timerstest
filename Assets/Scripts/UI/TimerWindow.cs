using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using RunasDev.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TimerWindow : AbstractWindow
{
    private Timer _timer;

    [SerializeField] private PressHandlerButton addTimeButton;
    [SerializeField] private PressHandlerButton subTimeButton;
    [SerializeField] private Button startButton;
    [SerializeField] private TMP_Text textTimer;

    [SerializeField] private float maxTimeChangeSpeed;
    [SerializeField] private float timerAcceleration;


    private TimerMenu _timerMenu;
    private float _timeChangeSpeed;

    public void Open(Timer timer, TimerMenu timerMenu)
    {
        _timerMenu = timerMenu;
        _timer = timer;
        _timeChangeSpeed = 0;
        base.Open();
    }

    protected override void OnOpen()
    {
        startButton.onClick.AddListener(() =>
        {
            _timer.Start();
            Close();
            _timerMenu.Open();
        });

        base.OnOpen();
    }

    private void Update()
    {
        textTimer.text = TimeSpan.FromSeconds(_timer.TimeLeft).ToString("hh\\:mm\\:ss");

        if (addTimeButton.Pressing || subTimeButton.Pressing)
            _timeChangeSpeed =
                Mathf.Clamp(_timeChangeSpeed + timerAcceleration * Time.deltaTime, 0, maxTimeChangeSpeed);
        else
        {
            _timeChangeSpeed = 0;
            return;
        }
        _timer.TimeLeft += (subTimeButton.Pressing? -1 : 1) * _timeChangeSpeed * Time.deltaTime;

    }

    protected override void OnClose()
    {
        startButton.onClick.RemoveAllListeners();
        base.OnClose();
    }
}