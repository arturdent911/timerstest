using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RunasDev.UI
{
    public class TimerMenu : AbstractWindow
    {
        [SerializeField] private GameObject timerButtonPrefab;
        [SerializeField] private Transform buttonPanel;
        
        private TimerHandler _timerHandler;

        private SetPool _pool;
        private TimerWindow _timerWindow;
        private List<MainMenuButton> _buttons;

        public void Init(TimerHandler timerHandler, TimerWindow timerWindow)
        {
            _timerWindow = timerWindow;
            _timerHandler = timerHandler;
            
            _pool ??= new SetPool(timerButtonPrefab);
            _buttons ??= new List<MainMenuButton>(timerHandler.Timers.Count);
        }
        
        public void Open()
        {
            base.Open();
        }

        protected override void OnOpen()
        {
            base.OnOpen();

            StartCoroutine(InitCoroutine());
        }

        private IEnumerator InitCoroutine()
        {
            foreach (var timer in _timerHandler.Timers)
            {
                var button = _pool.Spawn<MainMenuButton>(buttonPanel);
                button.transform.SetAsLastSibling();

                button.Init(timer);
                _buttons.Add(button);
                yield return new WaitForSeconds(0.3f);
            }

            foreach (var button in _buttons)
            {
                button.OnClick += (() => { StartCoroutine(OpenTimerWindow(button.Timer)); });
            }
        }
        
        private IEnumerator OpenTimerWindow(Timer timer)
        {
            foreach (var button in _buttons)
            {
                button.OnClick = null;
            }
            
            foreach (var button in _buttons)
            {
                button.DeInit();
                yield return new WaitForSeconds(0.3f);
            }
            _buttons.Clear();

            _timerWindow.Open(timer, this);
            Close();
        }
        
        protected override void OnClose()
        {
            foreach (var button in _buttons)
            {
                button.DeInit();
            }
            
            _buttons.Clear();
            _pool.DeSpawnAll();
            base.OnClose();
        }
    }
}