using System;
using UnityEngine;
using UnityEngine.EventSystems;
     
     
public class PressHandlerButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private bool _pressing = false;

    public bool Pressing => _pressing;

    public void OnPointerDown (PointerEventData eventData)
    {
        _pressing = true;
    }
    
    public void OnPointerUp (PointerEventData eventData)
    {
        _pressing = false;
    }
}